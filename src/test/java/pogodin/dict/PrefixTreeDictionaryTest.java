package pogodin.dict;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PrefixTreeDictionaryTest {

    @Test
    public void prefixTreeDictionaryShouldFindLongestWord() throws Exception {
        PrefixTreeDictionary dict = new PrefixTreeDictionary();
        for (String word : new String[]{"abba", "abawa", "aaw"}) {
            dict.addWord(word);
        }
        assertEquals("abba", dict.findLongestWord("aabb"));
        assertEquals("abba", dict.findLongestWord("aaabababababaaabb"));
        assertEquals("abawa", dict.findLongestWord("aabababababwwwbwbababawab"));
        assertEquals("abawa", dict.findLongestWord("baaaw"));
        assertEquals("", dict.findLongestWord("baaa"));
        assertEquals("", dict.findLongestWord("bbba"));
        assertEquals("aaw", dict.findLongestWord("wwaaa"));
        assertEquals("", dict.findLongestWord(""));
        assertEquals("", dict.findLongestWord("a"));
    }
}