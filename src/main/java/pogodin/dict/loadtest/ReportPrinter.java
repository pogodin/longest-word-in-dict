package pogodin.dict.loadtest;

import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

public class ReportPrinter {
    public static void printReport(String name, Metrics metrics) throws IOException {
        String fileNamePrefix = "report_" + new SimpleDateFormat("yyyyMMdd-HHmmss_").format(new Date()) + name + "_";
        String fileNameSuffix = ".txt";

        for (String metricName : metrics.allNames()) {
            OneMetric oneMetric = metrics.get(metricName);
            File file = new File(fileNamePrefix + metricName + fileNameSuffix);
            Files.write(generateReport(oneMetric), file, Charset.defaultCharset());
            System.out.println("metric " + metricName + " saved to file " + file.getAbsolutePath());
        }
    }

    private static String generateReport(OneMetric metric) {
        StringBuilder sb = new StringBuilder();

        sb.append("wordLen\\dictSize");
        metric.getDictSizes().forEach(dictSize -> sb.append(mkCell(Optional.of(dictSize.longValue()))));
        sb.append("\n");

        metric.getWordLenghts().forEach(wordLen -> {
            sb.append(String.format("%5d           ", wordLen));
            metric.getDictSizes().forEach(dictSize -> {
                sb.append(mkCell(metric.get(wordLen, dictSize)));
            });
            sb.append("\n");
        });
        return sb.toString();
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private static String mkCell(Optional<Long> valueOpt) {
        return String.format("%8s", valueOpt.map(Object::toString).orElse("?"));
    }

}
