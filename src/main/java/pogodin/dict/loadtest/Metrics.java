package pogodin.dict.loadtest;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Metrics {
    private final Map<String, OneMetric> metricsByName = new LinkedHashMap<>();

    public OneMetric get(String name) {
        return metricsByName.computeIfAbsent(name, n -> new OneMetric());
    }

    public Set<String> allNames() {
        return metricsByName.keySet();
    }
}
