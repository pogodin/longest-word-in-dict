package pogodin.dict.loadtest;

import pogodin.dict.Dictionary;

import java.util.function.Consumer;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

abstract class AbstractTest {
    protected final Metrics metrics = new Metrics();

    private final String name;
    protected final Dictionary dict;
    protected final IterParams wordLenIterParams;
    protected final IterParams dictSizeIterParams;

    AbstractTest(String name, Dictionary dict, IterParams wordLenIterParams, IterParams dictSizeIterParams) {
        this.name = name;
        this.dict = dict;
        this.wordLenIterParams = wordLenIterParams;
        this.dictSizeIterParams = dictSizeIterParams;
    }

    public abstract void doTest() throws Exception;

    public void start() throws Exception {
        doTest();
        ReportPrinter.printReport(name, metrics);
    }

    protected void iterateOverWordsLengths(Consumer<Integer> doIteration) throws Exception {
        iterate(doIteration, this.wordLenIterParams);
    }

    protected void iterateOverDictSizes(Consumer<Integer> doIteration) throws Exception {
        iterate(doIteration, this.dictSizeIterParams);
    }

    private void iterate(Consumer<Integer> doIteration, IterParams iterParams) {
        for (int len = iterParams.from; len <= iterParams.to; len = iterParams.increment(len)) {
            doIteration.accept(len);
        }
    }

    protected int measureLongestWordSearch(int charsNumber) {
        long dictMetric = 0;

        int findEachWordTimes = 10;
        for (int i = 0; i < findEachWordTimes; i++) {
            String chars = randomAlphabetic(charsNumber);

            long start = System.nanoTime();
            dict.findLongestWord(chars);
            dictMetric += (System.nanoTime() - start);
        }

        return (int) (dictMetric / findEachWordTimes / 1000000);
    }

    protected int getMemMb() {
        System.gc();
        try {
            Thread.sleep(500);
        } catch (InterruptedException ignored) {
        }

        return (int) (totalMemMb() - Runtime.getRuntime().freeMemory() / 1024 / 1024);
    }

    protected int totalMemMb() {
        return (int) (Runtime.getRuntime().totalMemory() / 1024 / 1024);
    }
}
