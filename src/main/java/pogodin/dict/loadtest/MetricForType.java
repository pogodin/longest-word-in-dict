package pogodin.dict.loadtest;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class MetricForType {

    private Map<Integer, Long> metricsByIndex = new LinkedHashMap<>();
    boolean isRecording = true;
    private final long recordingLimit;

    public MetricForType(long recordingLimit) {
        this.recordingLimit = recordingLimit;
    }

    public void record(int index, Callable<Long> doAction) throws Exception {
        if (isRecording) {
            Long metric = doAction.call();

            if (metric == null) {
                throw new IllegalStateException("method returned null");
            }

            metricsByIndex.put(index, metric);
            if (metric > recordingLimit) {
                isRecording = false;
            }
        }
    }
}
