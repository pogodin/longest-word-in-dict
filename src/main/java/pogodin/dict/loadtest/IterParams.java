package pogodin.dict.loadtest;

import java.util.function.Function;

public class IterParams {
    public final int from;
    public final int to;
    public final Function<Integer, Integer> step;

    public IterParams(int from, int to, Function<Integer, Integer> step) {
        this.from = from;
        this.to = to;
        this.step = step;
    }

    public int increment(int value) {
        return step.apply(value);
    }
}
