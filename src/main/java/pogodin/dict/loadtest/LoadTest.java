package pogodin.dict.loadtest;

import com.google.common.collect.Sets;
import pogodin.dict.Dictionary;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class LoadTest extends AbstractTest {
    public LoadTest(String name, Dictionary dict, IterParams wordLenIterParams, IterParams dictSizeIterParams) {
        super(name, dict, wordLenIterParams, dictSizeIterParams);
    }

    @Override
    public void doTest() throws Exception {
        System.out.println("=================== Memory test =====================");
        System.out.println("Total memory: " + totalMemMb());

        List<Integer> dictSizes = new ArrayList<>();
        System.out.print("  wl\\ds");
        iterateOverDictSizes(dictSize -> {
            dictSizes.add(dictSize);
            System.out.printf("%8d    ", dictSize);
        });
        System.out.println();

        iterateOverWordsLengths(wordLen -> {
            System.out.printf("%5d", wordLen);

            List<String> words = generateWords(dictSizeIterParams.to, wordLen);
            System.out.print(":");
            int initMem = getMemMb();

            try {
                for (Integer dictSize : dictSizes) {
                    addWordsToDictUpToSize(dictSize, words);

                    int mem = getMemMb() - initMem;
                    metrics.get("mem").add(wordLen, dictSize, mem);
                    System.out.printf(" %5d/", mem);

                    int timeMs = measureLongestWordSearch(wordLen);
                    metrics.get("time").add(wordLen, dictSize, timeMs);
                    System.out.printf("%-5d", timeMs);

                    if (timeMs > 10000) {
                        System.out.print(" !! too long");
                        if (dictSize.equals(dictSizes.get(0))) {
                            return;
                        }
                        break;
                    }
                }
            } catch (java.lang.OutOfMemoryError e) {
                System.out.print(" !!! Out Of Memory !!!");
            }

            dict.clear();
            System.out.println();
        });
    }

    private List<String> generateWords(int wordsNumber, Integer averageWordsLen) {
        HashSet<String> set = Sets.newHashSetWithExpectedSize(wordsNumber);
        while (set.size() < wordsNumber) {
            set.add(randomAlphabetic(averageWordsLen * 2 / 3, averageWordsLen * 4 / 3));
        }
        return new ArrayList<>(set);
    }

    private void addWordsToDictUpToSize(int dictSizeNeeded, List<String> words) {
        for (int i = dict.size(); dict.size() < dictSizeNeeded && i < words.size(); i++) {
            dict.addWord(words.get(i));
        }

        if (dict.size() != dictSizeNeeded) {
            System.out.printf("! dict size %d < %d", dict.size(), dictSizeNeeded);
        }
    }
}