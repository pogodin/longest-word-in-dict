package pogodin.dict.loadtest;

import java.util.HashMap;
import java.util.Map;

public class MetricsLine {
    private final long recordingLimit;
    private final Map<String, MetricForType> metricForTypeByName = new HashMap<>();

    public MetricsLine(long recordingLimit) {
        this.recordingLimit = recordingLimit;
    }

    public MetricForType get(String name) {
        return metricForTypeByName.computeIfAbsent(name, n -> new MetricForType(recordingLimit));
    }

    public boolean isStillRecording() {
        if (metricForTypeByName.isEmpty()) {
            return true;
        }

        for (MetricForType metricForType : metricForTypeByName.values()) {
            if (metricForType.isRecording) {
                return true;
            }
        }
        return false;
    }
}
