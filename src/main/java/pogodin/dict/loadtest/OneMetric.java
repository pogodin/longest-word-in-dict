package pogodin.dict.loadtest;

import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

public class OneMetric {
    private final Set<Integer> wordLenghts = new HashSet<>();
    private final Set<Integer> dictSizes = new HashSet<>();
    private final Map<Pair<Integer, Integer>, Long> grid = new HashMap<>();

    public void add(int wordLen, int dictSize, long value) {
        this.wordLenghts.add(wordLen);
        dictSizes.add(dictSize);
        grid.put(Pair.of(wordLen, dictSize), value);
    }

    public Optional<Long> get(int wordLen, int dictSize) {
        return Optional.ofNullable(grid.get(Pair.of(wordLen, dictSize)));
    }

    public Set<Integer> getWordLenghts() {
        return new TreeSet<>(wordLenghts);
    }

    public Set<Integer> getDictSizes() {
        return new TreeSet<>(dictSizes);
    }
}
