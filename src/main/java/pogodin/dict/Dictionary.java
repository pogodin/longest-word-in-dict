package pogodin.dict;

public interface Dictionary extends Iterable<String> {
    void clear();

    void addWord(String word);

    String findLongestWord(String chars);

    int size();
}
