package pogodin.dict;

import org.apache.commons.lang3.Validate;

public class Utils {
    public static String dropChar(String string, int i) {
        Validate.isTrue(i >= 0 && i < string.length(), "can't drop char %d from string %s", i, string);
        return (i == 0)
                ? (string.length() == 1 ? "" : string.substring(1))
                : (i == string.length() - 1)
                ? string.substring(0, i)
                : string.substring(0, i) + string.substring(i + 1, string.length());
    }
}
