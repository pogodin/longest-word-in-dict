package pogodin.dict.misc;

import pogodin.dict.Dictionary;
import pogodin.dict.ListDictionary;
import pogodin.dict.PrefixTreeDictionary;

import java.util.Scanner;

public class Interactive {
    private final Dictionary dict1 = new PrefixTreeDictionary();
    private final Dictionary dict2 = new ListDictionary();

    public void start() {
        System.out.println("Input dictionary: ");
        Scanner scanner = new Scanner(System.in);
        for (;;) {
            String word = scanner.nextLine();
            if (word.equals("stop")) {
                break;
            }
            dict1.addWord(word);
            dict2.addWord(word);
        }

        System.out.println("input letters:");
        for (;;) {
            String chars = scanner.nextLine();
            System.out.println("longest word1: " + dict1.findLongestWord(chars));
            System.out.println("longest word2: " + dict2.findLongestWord(chars));
        }
    }
}
