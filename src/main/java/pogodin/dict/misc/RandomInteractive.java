package pogodin.dict.misc;

import org.apache.commons.lang3.StringUtils;
import pogodin.dict.Dictionary;
import pogodin.dict.ListDictionary;
import pogodin.dict.PrefixTreeDictionaryOld;

import java.util.Scanner;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class RandomInteractive {
    Dictionary dict1 = new PrefixTreeDictionaryOld();
    Dictionary dict2 = new ListDictionary();

    Scanner scanner = new Scanner(System.in);

    public void start() {
        System.out.print("Input min word length: ");
        int minLen = scanner.nextInt();

        System.out.print("Input max word length: ");
        int maxLen = scanner.nextInt();
        if (maxLen < minLen) throw new IllegalArgumentException("max len can't be < min len");

        System.out.print("Input number of words in dictionary: ");
        int wordsNumber = scanner.nextInt();

        for (int i = 0; i < wordsNumber; i++) {
            String word = randomAlphabetic(minLen, maxLen);
            dict1.addWord(word);
            dict2.addWord(word);
        }


        for (;;) {
            System.out.print("Input number of words to search in dictionaries: ");
            wordsNumber = scanner.nextInt();

            long time1 = 0;
            long time2 = 0;

            for (int i = 0; i < wordsNumber; i++) {
                long start = System.nanoTime();
                String word = randomAlphabetic(minLen, maxLen);

                String longest1 = dict1.findLongestWord(word);
                time1 += (System.nanoTime() - start) / 1000;

                start = System.nanoTime();
                String longest2 = dict2.findLongestWord(word);
                time2 += (System.nanoTime() - start) / 1000;

                if (!StringUtils.equals(longest1, longest2)) {
                    System.out.println("Longest mismatch for word " + word + ": " +
                            "dict1 = " + longest1 + ", dict2 = " + longest2);
                }
            }
            System.out.println("Time elapsed: dict1 = " + time1 / 1000 + "ms, dict2 = " + time2 / 1000 + "ms");
        }
    }
}
