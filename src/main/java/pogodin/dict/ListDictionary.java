package pogodin.dict;

import com.google.common.collect.Iterators;
import org.apache.commons.lang3.Validate;

import java.util.*;

public class ListDictionary implements Dictionary {
    private final List<WordsWithSameLen> wordsList;

    public ListDictionary() {
        wordsList = new ArrayList<>();
        wordsList.add(new WordsWithSameLen(""));
    }

    @Override
    public void clear() {
        wordsList.clear();
        wordsList.add(new WordsWithSameLen(""));
    }

    @Override
    public void addWord(String word) {
        getWordsWithLen(word.length()).add(word);
    }

    private WordsWithSameLen getWordsWithLen(int len) {
        extendWordsList(len);
        return wordsList.get(len);
    }

    private void extendWordsList(int indexToBeAvailable) {
        if (indexToBeAvailable >= wordsList.size()) {
            for (int i = wordsList.size(); i <= indexToBeAvailable; i++) {
                wordsList.add(new WordsWithSameLen(i));
            }
        }
    }

    @Override
    public String findLongestWord(String chars) {
        int len = chars.length();

        for (int i = len; i >= 0; i--) {
            String word = getWordsWithLen(i).findWordWhichCanBeCreatedFrom(chars);
            if (word != null) {
                return word;
            }
        }

        throw new IllegalStateException("this should never happen. In the worst case an empty string should be returned");
    }

    @Override
    public Iterator<String> iterator() {
        return Iterators.concat(wordsList.stream().map(WordsWithSameLen::iterator).iterator());
    }

    @Override
    public int size() {
        return wordsList.stream().mapToInt(w -> w.words.size()).sum();
    }

    private static class WordsWithSameLen implements Iterable<String> {
        private List<Word> words = new ArrayList<>();
        private int len;

        public WordsWithSameLen(int len) {
            this.len = len;
        }

        public WordsWithSameLen(String... words) {
            Validate.isTrue(words.length > 0, "this constructor can't be invoked with no args");
            len = words[0].length();

            for (String word : words) {
                add(word);
            }
        }

        public void add(String word) {
            Validate.isTrue(word.length() == len, "word with len %d expected but got: %s", len, word);
            words.add(new Word(word));
        }

        public String findWordWhichCanBeCreatedFrom(String chars) {
            for (Word word : words) {
                if (word.canBeCreatedFrom(chars)) {
                    return word.word;
                }
            }
            return null;
        }

        @Override
        public Iterator<String> iterator() {
            return words.stream().map(w -> w.word).iterator();
        }
    }

    private static class Word {
        final String word;
        final Map<Character, Integer> charsCounts = new HashMap<>();

        public Word(String word) {
            this.word = word;
            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                charsCounts.put(c, charsCounts.containsKey(c) ? charsCounts.get(c) + 1 : 1);
            }
        }

        public boolean canBeCreatedFrom(String chars) {
            // TODO: would be nice to make a copy immediately, without inserting every element, which involves hashing
            Map<Character, Integer> m = new HashMap<>(charsCounts);

            for (int i = 0; i < chars.length(); i++) {
                char c = chars.charAt(i);

                Integer count = m.get(c);
                if (count != null) {
                    if (count > 1) {
                        m.put(c, count - 1);
                    } else {
                        m.remove(c);
                    }
                }
            }
            return m.isEmpty();
        }
    }
}
