package pogodin.dict;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PrefixTreeDictionary implements Dictionary {
    private Node root;
    private int size;

    public PrefixTreeDictionary() {
        clear();
    }

    @Override
    public void clear() {
        root = new Node();
        size = 0;
    }

    @Override
    public void addWord(String word) {
        Node node = root;
        for (int i = 0; i < word.length(); i++) {
            char letter = word.charAt(i);
            node = node.children.computeIfAbsent(letter, k -> new Node());
        }

        if (!node.isLeaf) size++;
        node.isLeaf = true;
    }

    @Override
    public String findLongestWord(String chars) {
        return find(root, "", chars);
    }

    private String find(Node parentNode, String prefix, String chars) {
        LongestWord longestWord = new LongestWord();

        for (int i = 0; i < chars.length(); i++) {
            char curChar = chars.charAt(i);

            Node curNode = parentNode.children.get(curChar);
            if (curNode == null) {
                continue;
            }

            if (curNode.isLeaf) {
                longestWord.putIfBigger(prefix + curChar);
            }

            String curLongestWord = find(curNode, prefix + curChar, Utils.dropChar(chars, i));
            longestWord.putIfBigger(curLongestWord);

            // optimization: if a word with all chars found, that's it
            if (curLongestWord.length() == prefix.length() + chars.length()) {
                break;
            }
        }
        return longestWord.get();
    }

    @Override
    public Iterator<String> iterator() {
        throw new UnsupportedOperationException("PrefixTreeDictionaryOld::iterator is unsupported yet");
    }

    @Override
    public int size() {
        return size;
    }

    private static class Node {
        boolean isLeaf = false;
        final Map<Character, Node> children = new HashMap<>();

        @Override
        public String toString() {
            return "Node(isLeaf=" + isLeaf + "," + children.size() + ")";
        }
    }
}
