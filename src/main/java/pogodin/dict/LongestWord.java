package pogodin.dict;

public class LongestWord {
    private String theWord = "";

    public boolean putIfBigger(String word) {
        if (word.length() > theWord.length()) {
            theWord = word;
            return true;
        }
        return false;
    }

    public String get() {
        return theWord;
    }
}
