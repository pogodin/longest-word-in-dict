package pogodin.dict;

import pogodin.dict.loadtest.IterParams;
import pogodin.dict.loadtest.LoadTest;
import pogodin.dict.misc.Interactive;
import pogodin.dict.misc.RandomInteractive;

public class Main {
    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.printf("command is missing");
        }

        if ("interactive".equals(args[0])) {
            new Interactive().start();
        } else if ("random".equals(args[0])) {
            new RandomInteractive().start();
        } else if ("loadtest_tree".equals(args[0])) {
            new LoadTest("tree", new PrefixTreeDictionary(),
                    new IterParams(5, 100, l -> l + 5),
                    new IterParams(1024, 1024 * 1024, s -> s * 2)
            ).start();
        } else if ("loadtest_list".equals(args[0])) {
            new LoadTest("list", new ListDictionary(),
                    new IterParams(5, 100, l -> l + 5),
                    new IterParams(1024, 1024 * 1024, s -> s * 2)
            ).start();
        } else {
            System.out.printf("unknown command: " + args[0]);
        }
    }

}
