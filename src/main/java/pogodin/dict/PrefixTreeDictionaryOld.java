package pogodin.dict;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PrefixTreeDictionaryOld implements Dictionary {
    private Node root;
    private int size;

    public PrefixTreeDictionaryOld() {
        clear();
    }

    @Override
    public void clear() {
        root = new Node('0');
        size = 0;
    }

    @Override
    public void addWord(String word) {
        Node node = root;
        for (int i = 0; i < word.length(); i++) {
            char letter = word.charAt(i);

            Node child = node.findChild(letter);
            if (child == null) {
                child = new Node(letter);
                node.children.add(child);
            }
            node = child;
        }

        if (!node.isLeaf) size++;
        node.isLeaf = true;
    }

    @Override
    public String findLongestWord(String chars) {
        return find(root, "", chars);
    }

    private String find(Node parentNode, String prefix, String chars) {
        LongestWord longestWord = new LongestWord();

        char[] charArr = chars.toCharArray();
        for (int i = 0; i < charArr.length; i++) {
            char curChar = charArr[i];

            Node curNode = parentNode.findChild(curChar);
            if (curNode == null) {
                continue;
            }

            if (curNode.isLeaf) {
                longestWord.putIfBigger(prefix + curChar);
            }

            String curLongestWord = find(curNode, prefix + curChar, Utils.dropChar(chars, i));
            longestWord.putIfBigger(curLongestWord);

            // optimization: if a word with all chars found, that's it
            if (curLongestWord.length() == prefix.length() + chars.length()) {
                break;
            }
        }
        return longestWord.get();
    }

    @Override
    public Iterator<String> iterator() {
        throw new UnsupportedOperationException("PrefixTreeDictionaryOld::iterator is unsupported yet");
    }

    @Override
    public int size() {
        return size;
    }

    private static class Node {
        final char letter;
        boolean isLeaf = false;
        final List<Node> children = new ArrayList<>(52);

        public Node(char letter) {
            this.letter = letter;
        }

        public Node findChild(char letter) {
            for (int i = 0; i < children.size(); i++) {
                if (children.get(i).letter == letter) {
                    return children.get(i);
                }
            }
            return null;
        }

        @Override
        public String toString() {
            return "Node(isLeaf=" + isLeaf + "," + children.size() + ")";
        }
    }
}
