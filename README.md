# Longest word exercise #

This repository contains 2 solutions of the following exercise: find in the given dictionary a longest word, which can be combined from the given characters. The solutions are also load tested for different words lengths and dictionary sizes.

## Build ##

`mvn clean package`

## Load test ##

#### for prefix tree dictionary solution ####

`java -Xmx10g -jar target/dict-1.0-SNAPSHOT.jar loadtest_tree`

#### for list dictionary solution ####

`java -Xmx10g -jar target/dict-1.0-SNAPSHOT.jar loadtest_list`